
import numpy as np
import cv2

cap = cv2.VideoCapture("test.mov")

# Define the codec and create VideoWriter object
#fourcc = cv2.VideoWriter_fourcc(*'XVID')
fourcc = cv2.cv.CV_FOURCC(*'mp4v')
out = cv2.VideoWriter('output.avi',fourcc, 15.0, (720,480), True)
print dir(out)
#print out.get

while(cap.isOpened()):
    ret, frame = cap.read()
    #print out.isOpened()
    if ret==True:
        frame = cv2.flip(frame,0)

        # write the flipped frame
        out.write(frame)


        #cv2.imshow('frame',frame)
        #if cv2.waitKey(1) & 0xFF == ord('q'):
            #break
    else:
        break

# Release everything if job is finished
cap.release()
out.release()
cv2.destroyAllWindows()
