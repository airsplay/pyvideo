
import cv2
from cv2 import VideoCapture
import cv2.cv as cv
import numpy as np
import os
import json # For formatted printing

# A simple wrapper of cv2.VideoWriter
class VideoWriterth:
	def __init__(self, *args, **kwargs):		
		"""
		If there is any arguments in the argument_list,
		using these argument to initilize the underlying VideoWriter
		"""
		self.writer = cv2.VideoWriter()
		if not (len(args) == 0 and len(kwargs) == 0):
			self.set(*args, **kwargs)
		#if not video == "":
			#self.set(video)

	def set(self, video = "", mode = "mp4v", fps = 20.0, width = 640, height = 480):
		"""
		set the video
		"""
		#self.fourcc = cv2.VideoWriter_fourcc(*'MJPG')
		#Note: It doesn't work here 
		self.fourcc = cv.CV_FOURCC(*mode)
		self.writer.open(video, self.fourcc, fps, (width,height))
		if not self.writer.isOpened():
			raise "Can't open the video"
	
	def write(self, frame):
		"""
		write a frame to the movie file
		"""
		#print frame
		self.writer.write(frame)

	def write_folder(self, folder):
		for img_name in os.listdir(folder):
			self.write(cv2.imread(img_name))
		
	def release(self):
		self.writer.release()
if __name__ == "__main__":
	w = VideoWriterth()
	w.set("test_out.mov")
