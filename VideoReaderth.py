import cv2
#from cv2 import VideoCapture
import cv2.cv as cv
import numpy as np
import os
import json # For formatted printing

# A simple wraper of cv2.VideoCaputre
#I don't know why the python doesn't allow me to direct inherient from 
#cv2.VideoCapture, so I use this as a member. 
class VideoReaderth:
	

	
	# Init the class
	def __init__(self, video = ""):
		self.cap = cv2.VideoCapture()
		if not video == "":
			self.set(video)
	
	# set the video to the parameter video
	def set(self, video = ""):
		self.cap.release()
		if not os.path.exists(video):
			print "ERROR:No Such Video"
		self.cap.open(video)
		self.statistics()
		if not(self.cap.isOpened()):
			print "Can't open the video"
	
	# Return the statistics of the video
	def statistics(self):
		self.stat = {}
		#self.stat[frames] = self.cap.get(cv.CV_CAP_PROP_FRAME_COUNT)
		stat_feature = "CV_CAP_PROP_" 
		for attr in dir(cv):
			if "CV_CAP_PROP_" in attr:
				stat_name = attr[len(stat_feature):]
				self.stat[stat_name] = int(round(self.cap.get(getattr(cv, attr, "No_such_value"))))
		self.shape = (self.stat['FRAME_COUNT'], self.stat['FRAME_HEIGHT'], self.stat['FRAME_WIDTH'], 3)
		#print json.dumps(self.stat, indent = 1)
		return self.stat

	# print the statistics using json
	def show_statistics(self):
		print json.dumps(self.statistics(), indent = 1)
	

	# Read a frame from the video
	def read(self):
		return self.cap.read()
	
	# Show the video using openCV
	# Press "q" to quit it
	def show(self):
		for ret, frame in self:
			cv2.imshow("Video", frame)
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break
		
		cv2.destroyAllWindows()
		self.cap.set(cv.CV_CAP_PROP_POS_FRAMES, 0) # Se the video to the start
		
	
	# Make this class an Iterator, call read_frame each time
	# Sample usage:
	# for frame in v:
	#	print frame
	def __iter__(self):
		return self

	# Next frame in iteration
	def next(self):
		frame = self.read()
		#print frame[0]
		if frame[0]:
			return frame
		else:
			self.cap.set(cv.CV_CAP_PROP_POS_FRAMES, 0)
			raise StopIteration()

	def release(self):
		self.cap.release()

if __name__ == "__main__":
	v = VideoReaderth()
	v.set("test.mov")
	#v.show()
	v.show_statistics()
	#for frame in v:
		#pass	
	#frame = v.read()
	#v.print_statistics()
	#v.show()
