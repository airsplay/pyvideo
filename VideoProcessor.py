from VideoReaderth import VideoReaderth
from VideoWriterth import VideoWriterth
import cv2
import cv2.cv as cv
import numpy as np
import os
from hairTools import *

class VideoProcessor:
	def __init__(self, input_video_name = "", output_video_name = ""):
		self.output_video_name = output_video_name
		if input_video_name == "":
			self.reader = VideoReaderth()
		else:
			self.set_input(input_video_name)
	
	def set_input(self, input_video_name):
		self.reader = VideoReaderth(input_video_name)
		self.shape_input = self.reader.shape
		
	
	
	def show_input(self):
		self.reader.show()
	def set_output(self, output_video_name):
		self.output_video_name = output_video_name
	
	def save_slice(self, output_folder, step = 1, method = "vertical"):
		"""
		Save the slice of the input_video.
		:param output_folder: where to save the result
		:param step: The step of slices.
		:param method: "vertical" or "horizon"
		"""
		check_or_create(output_folder)
		tensor = np.zeros(self.reader.shape, dtype = np.uint8)
		cnt = 0		
		for ret,frame in self.reader:
			tensor[cnt] = frame
			cnt += 1
		t, h, w, c = self.reader.shape
		if method == "vertical":
			for iter_h in xrange(h):
				img_h = tensor[:,iter_h,...]
				cv2.imwrite(os.path.join(output_folder, ("%04d" % (iter_h)) + ".jpg"), img_h)
		if method == "horizon":
			for iter_w in xrange(w):
				img_w = tensor[:,:, iter_w, :]
				cv2.imwrite(os.path.join(output_folder, ("%04d" % (iter_w) + ".jpg")), img_w)
			
		#frame_list = [frame for ret,frame in self.reader]
		#frame_nparray = np.array(frame_list)
		#print frame_nparray
			
	def process(self, transfer = lambda x:x, if_show = False):
		"""
		Transfer each frame with the transfer function in parameter 
			param transfer is init to the fixed-point function
		:param transfer: The transfer function. labmda x:y(x)
		:param if_show: If show the output video during processing
		:return: None
		Press q to exit the processing.
		"""
		if not self.output_video_name == "":
			#Use the first frame of input to determine how to set the video size			
			ret, frame = self.reader.read()
			frame = transfer(frame)
			self.writer = VideoWriterth()
			input_stat = self.reader.statistics()
			self.writer.set(self.output_video_name, "mp4v", input_stat["FPS"], frame.shape[1], frame.shape[0])
		else:
			raise "Didn't set the output video's name"
		
		for ret, frame in self.reader:
			out_frame = transfer(frame)
			if if_show:
				cv2.imshow('frame', out_frame)
			self.writer.write(out_frame)
			
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break
		
		self.writer.release()
		cv2.destroyAllWindows()

	def batch_process(self, transfer, if_show = True):
		"""
		Transfer each frame with the transfer function in parameter 
			param transfer is init to the fixed-point 
			Transfer is an function it
		:param transfer: The transfer function
			transfer = lambda image:if_ended, out_images
			If if_ended == False, out_images must be None.
			If if_ended == True, out_image must be the frames
		:param if_show: If show the output video during processing
		:return: None
		Press q to exit the processing.
		"""
		if not self.output_video_name == "":
			#Use the first frame of input to determine how to set the video size			
			ret, frame = self.reader.read()
			frame = transfer(frame)
			self.writer = VideoWriterth()
			input_stat = self.reader.statistics()
			self.writer.set(self.output_video_name, "mp4v", input_stat["FPS"], frame.shape[1], frame.shape[0])
		else:
			raise "Didn't set the output video's name"
		
		for ret, frame in self.reader:
			is_end, out_frames = transfer(frame)
			if is_end:
				for out_frame in out_frames:
					self.writer.write(out_frame)

			if if_show:
				cv2.imshow('frame', out_frame)
			#self.writer.write(out_frame)

			if cv2.waitKey(1) & 0xFF == ord('q'):
				break
		
		is_end, out_frames = transfer(None)
		
		self.writer.release()
		cv2.destroyAllWindows()
if __name__ == "__main__":
	p = VideoProcessor("2013_dynamichair.mkv", "1.avi")
	#p.show_input()
	p.process()


